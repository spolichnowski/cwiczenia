import os

def zadanie_2():
    #podaj lokalizacje folderu

    file_list = os.listdir("/home/staszek/lekcje1/zdjecia")
    saved_path = os.getcwd()
    print("Current Working Directory is" + saved_path)   
    os.chdir("/home/staszek/lekcje1/zdjecia")
    
    for file_name in file_list :
        print("Old name-" + file_name)
        print("New name-" + file_name.translate(None, '0123456789'))        
        os.rename(file_name, file_name.translate(None, '0123456789'))
    os.chdir(saved_path)
    

zadanie_2()
